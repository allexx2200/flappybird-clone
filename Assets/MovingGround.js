﻿#pragma strict
private var posy : float = -1f;
private var respawnx : float = 1.53f;

var speed : float = 5f;

function Start () {
	this.transform.position.y = posy;
}

function Update () {
	this.transform.position.x -= Time.deltaTime * speed;
	if(this.transform.position.x < -respawnx){
		this.transform.position.x = respawnx;
	}
}