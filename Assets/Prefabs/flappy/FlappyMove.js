﻿#pragma strict

private var speedy : float = -0.7f;
private var isDead : boolean = false;

private var maxAngle : float = -90f;
private var rotationSpeed : float = 0.1f;

private var timerValue : float = 0.1f;
private var timer : float = 0.1f;

private var releasedButton : boolean = true;

var death : AudioClip;
var swingSound : AudioClip;

private var titleScreen : boolean = true;

var stuffToDestroy1 : GameObject;
var stuffToDestroy2 : GameObject;
var stuffToDestroy3 : GameObject;
var stuffToDestroy4 : GameObject;
var stuffToDestroy5 : GameObject;

function Start () {
	rigidbody2D.gravityScale = 0f;
}

function Update () {
	if(titleScreen){
	}


	if(isDead){
		transform.position.x = 0f;
		transform.position.y = -0.65f;
		return;
	}
	if (Input.touchCount >= 1 && releasedButton) {
		if(titleScreen){
			StartGame();
		}
		rigidbody2D.velocity = Vector2(0f,2f);
		audio.PlayOneShot(swingSound,1);
		releasedButton = false;
		timer = timerValue;
    } else if(Input.touchCount == 0) {
		timer -= Time.deltaTime;
		if(timer <= 0){
			releasedButton = true;
		}
	}
}

function OnTriggerEnter2D(other: Collider2D){
	if(!isDead){
		audio.PlayOneShot(death,1);
		isDead = true;
		Destroy(GetComponent("Animator"));
		Destroy(GetComponent("RigidBody 2D"));
		Destroy(GetComponent("Box Collider 2D"));
		yield WaitForSeconds (1);
		Application.LoadLevel(Application.loadedLevel);
	}
}

function StartGame(){
	titleScreen = false;
	rigidbody2D.gravityScale = 0.5f;
	Destroy(stuffToDestroy1);
	Destroy(stuffToDestroy2);
	Destroy(stuffToDestroy3);
	Destroy(stuffToDestroy4);
	Destroy(stuffToDestroy5);
}